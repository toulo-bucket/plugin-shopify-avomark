import { Layout, Card, Form, FormLayout, TextField, Button, Checkbox, Link, DatePicker, Stack } from "@shopify/polaris";
import React, {useCallback, useState} from 'react';

export default function inscription () {

  const [nom, setNom] = useState('');
  const handleNomChange = useCallback((newValue) => setNom(newValue), []);
  
  const [prenom, setPrenom] = useState('');
  const handlePrenomChange = useCallback((newValue) => setPrenom(newValue), []);
  
  const [email, setEmail] = useState('');
  const handleEmailChange = useCallback((newValue) => setEmail(newValue), []);
  
  const [password, setPassword] = useState('');
  const handlePasswordChange = useCallback((newValue) => setPassword(newValue), []);

  const [{month, year}, setDate] = useState({month: 11, year: 2021});
  const [selectedDates, setSelectedDates] = useState({
    start: new Date('Tue Dec 07 2021 00:00:00 GMT+100 (EST)'),
    end: new Date('Tue Dec 07 2021 00:00:00 GMT+100 (EST)'),
  });
  const handleMonthChange = useCallback((month, year) => setDate({month, year}), [], );

  const [checked1, setChecked1] = useState(false);
  const handleOffreChange = useCallback((newChecked) => setChecked1(newChecked), []);

  const [checked2, setChecked2] = useState(false);
  const handleConfidentialiteChange = useCallback((newChecked) => setChecked2(newChecked), []);

  const [checked3, setChecked3] = useState(false);
  const handleNewsletterChange = useCallback((newChecked) => setChecked3(newChecked), []);

  const [checked4, setChecked4] = useState(false);
  const handleConditionsChange = useCallback((newChecked) => setChecked4(newChecked), []);

  const [checked5, setChecked5] = useState(false);
  const handleFideliteChange = useCallback((newChecked) => setChecked5(newChecked), []);


  const handleSubmit = useCallback((_event) => {setEmail(''); setPassword('');}, []);
  

  return (
  <Layout>
    <Stack alignment="center" distribution="center">
    <Layout.Section>
      <Card title="Créez votre compte" sectioned>
        <Form onSubmit={handleSubmit}>
          <FormLayout>

          <Link url="abysterpartner.myshopify.com/admin/apps/appavomark" external>Vous avez déjà un compte ? Connectez vous !</Link>
  
          <TextField
            value={prenom}
            onChange={handlePrenomChange}
            label="Prénom"
            type="text"
            autoComplete="off"
          />
  
          <TextField
            value={nom}
            onChange={handleNomChange}
            label="Nom"
            type="text"
            autoComplete="off"
          />
  
          <TextField
            value={email}
            onChange={handleEmailChange}
            label="Email"
            type="email"
            autoComplete="email"
          />
  
          <TextField
            value={password}
            onChange={handlePasswordChange}
            label="Mot de passe"
            type="password"
          />
  
          <p>Date de naissance</p>
          <DatePicker
              month={month}
              year={year}
              onChange={setSelectedDates}
              onMonthChange={handleMonthChange}
              selected={selectedDates}
          />
  
          <Checkbox
              label="Recevoir les offres de nos partenaires"
              checked={checked1}
              onChange={handleOffreChange}
          />
  
          <Checkbox
              label="Message concernant la confidentialité des données clients
              conformément aux dispositions de la loi du n°78-17 du 6 Janvier 1978,
              vous disposez d'un droit d'accès, de rectification et d'apposition
              sur les données nominatives vous concernant."
              checked={checked2}
              onChange={handleConfidentialiteChange}
          />
  
          <Checkbox
              label="Recevoir notre newsletter"
              checked={checked3}
              onChange={handleNewsletterChange}
          />
          <p>Vous pouvez vous désinscrire à tout moment, vous trouverez pour celà 
              nos informations de contact dans les conditions d'utilisation du site.
          </p>
  
          <Checkbox
              label="J'accepte les conditions générales et la politique de confidentialité."
              checked={checked4}
              onChange={handleConditionsChange}
          />
  
          <p>Avantages & Fidélité </p>
          <Checkbox
              label="Je souhaite adhérer à la fidélité ou je suis déjà membre du programme."
              checked={checked5}
              onChange={handleFideliteChange}
          />
  
          <Button primary submit>ENREGISTRER</Button>
  
          </FormLayout>
        </Form>
        <></>
      </Card>
    </Layout.Section>
    </Stack>
  </Layout>
  );
}

