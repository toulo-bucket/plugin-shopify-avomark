import {Layout, Card, Form, FormLayout, TextField, Button, Link, Stack } from "@shopify/polaris";
import React, {useCallback, useState} from 'react';

export default function Index () {

  const [email, setEmail] = useState('');
  const handleEmailChange = useCallback((newValue) => setEmail(newValue), []);

  const [password, setPassword] = useState('');
  const handlePasswordChange = useCallback((newValue) => setPassword(newValue), []);

  const handleSubmit = useCallback((_event) => {setEmail(''); setPassword('');}, []);
  

  return (
  <Layout>
    <Stack alignment="center" distribution="center">
    <Layout.Section>
      <Card title="Connectez-vous à votre compte" sectioned>
        <Form onSubmit={handleSubmit}>
          <FormLayout>
            
          <TextField
            value={email}
            onChange={handleEmailChange}
            label="Email"
            type="email"
            autoComplete="email"
            max="1"
          />
  
          <TextField
            value={password}
            onChange={handlePasswordChange}
            label="Mot de passe"
            type="password"
            max="1"
          />
  
          
          <Link url="https://help.shopify.com/manual" external>Mot de pase oublié ?</Link>
          
          
          <Button primary submit>CONNEXION</Button>
          
          
          <Link url="abysterpartner.myshopify.com/admin/apps/appavomark/inscription" external>Pas de compte ? Créez en un !</Link>
          
          </FormLayout>
        </Form>
        <></>
      </Card>
    </Layout.Section>
    </Stack>
  </Layout>
  );
}

