import { Layout, Card, Form, FormLayout, TextField, Button, ButtonGroup, Checkbox, Stack} from "@shopify/polaris";
import React, {useCallback, useState} from 'react';

export default function Fidelite () {

  const [email, setEmail] = useState('');
  const handleEmailChange = useCallback((newValue) => setEmail(newValue), []);

  const [telfixe, setTelfixe] = useState('');
  const handleTelfixeChange = useCallback((newValue) => setTelfixe(newValue), []);

  const [telmobile, setTelmobile] = useState('');
  const handleTelmobileChange = useCallback((newValue) => setTelmobile(newValue), []);

  const [adresse, setAdresse] = useState('');
  const handleAdresseChange = useCallback((newValue) => setAdresse(newValue), []);

  const [adresse2, setAdresse2] = useState('');
  const handleAdresse2Change = useCallback((newValue) => setAdresse2(newValue), []);

  const [codepostal, setCodepostal] = useState('');
  const handleCodepostalChange = useCallback((newValue) => setCodepostal(newValue), []);

  const [ville, setVille] = useState('');
  const handleVilleChange = useCallback((newValue) => setVille(newValue), []);

  const [checked1, setChecked1] = useState(false);
  const handleSmsChange = useCallback((newChecked) => setChecked1(newChecked), []);

  const [checked2, setChecked2] = useState(false);
  const handleMailChange = useCallback((newChecked) => setChecked2(newChecked), []);


  return (
  <Layout>
    <Stack alignment="center" distribution="center">
    <Layout.Section>
      <Card title="Programme de fidélité" sectioned>
        <Form>
          <FormLayout>
          <p> Merci de compléter le formulaire ci dessous</p>
          <TextField
            value={email}
            onChange={handleEmailChange}
            label="Email"
            type="email"
            autoComplete="email"
          />
  
          <TextField
            value={telfixe}
            onChange={handleTelfixeChange}
            label="Téléphone fixe *"
            type="tel"
            autoComplete="off"
          />
  
          <TextField
            value={telmobile}
            onChange={handleTelmobileChange}
            label="Téléphone mobile *"
            type="tel"
            autoComplete="off"
          />
  
          <TextField
            value={adresse}
            onChange={handleAdresseChange}
            label="Adresse *"
            type="text"
            autoComplete="off"
          />
  
          <TextField
            value={adresse2}
            onChange={handleAdresse2Change}
            label="Adresse [2]"
            type="text"
            autoComplete="off"
          />
  
          <TextField
            value={codepostal}
            onChange={handleCodepostalChange}
            label="Code postal *"
            type="text"
            autoComplete="off"
          />
  
          <TextField
            value={ville}
            onChange={handleVilleChange}
            label="Ville *"
            type="text"
            autoComplete="off"
          />
  
  
          <Checkbox
              label="Recevoir les offres et avantages fidélité par SMS"
              checked={checked1}
              onChange={handleSmsChange}
          />
  
          <Checkbox
              label="Recevoir les offres et avantages fidélité par Email"
              checked={checked2}
              onChange={handleMailChange}
          />
          <p> * obligatoire pour recevoir vos offres</p>
  
          <ButtonGroup>
              <Button primary>Enregistrer</Button>
              <Button destructive>Se désabonner</Button>
          </ButtonGroup>
  
          </FormLayout>
        </Form>
        <></>
      </Card>
    </Layout.Section>
    </Stack>
  </Layout>
  );
}

